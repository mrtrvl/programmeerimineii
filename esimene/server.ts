const viewsFolder = 'views';

const express = require ('express');
const app = express();
const ejs = require('ejs');

let myPort = 3000;

app.set('view engine', 'ejs');

app.get('/', (req : any, res : any) => {
    res.locals.age = 20;
    res.render('index', {name: 'Juku'});
});

app.get('/pages', (req : any, res : any) => {
    res.render('pages/index');
});

app.listen(myPort, () => {
    console.log('listening on port ' + myPort);
});